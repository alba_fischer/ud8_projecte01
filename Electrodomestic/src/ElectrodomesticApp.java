import dto.Electrodomestic;

public class ElectrodomesticApp {

	public static void main(String[] args) {
	// Comprovem que funcioni imprimint els objectes per pantalla
		
		Electrodomestic e1 = new Electrodomestic();
		Electrodomestic e2 = new Electrodomestic(160.5, 20.1 );
		Electrodomestic e3 = new Electrodomestic(160.5, "ROIG", 'K',20.1);
				
		System.out.println("Usem els diferents constructors de l'exercici:");
		System.out.println(e1);
		System.out.println(e2);
		System.out.println(e3);
		
	}

}
