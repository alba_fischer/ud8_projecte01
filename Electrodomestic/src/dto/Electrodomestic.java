package dto;

public class Electrodomestic {
	//Atributs
	
	private double preuBase;
	private String color;
	private char consumEnergetic;
	private double pes;
	
	//CONSTRUCTORS
	
	//Constructor per defecte	
	public Electrodomestic() {
		this.preuBase = 100;
		this.color = "blanc";
		this.consumEnergetic = 'F';
		this.pes = 5;
	}
		
	//Constructor amb preu i pes. La resta per defecte.
	public Electrodomestic(double preuBase, double pes) {
		this.preuBase = preuBase;
		this.pes = pes;
		this.color = "blanc";
		this.consumEnergetic = 'F';
	}

	//Constructor amb tots els atributs.
	public Electrodomestic(double preuBase, String color, char consumEnergetic, double pes) {		
		this.preuBase = preuBase;
		this.color = comprovarColor(color);
		this.consumEnergetic = comprovarConsum(consumEnergetic);
		this.pes = pes;	
	}
	
	public static char comprovarConsum (char consEner) {
		//Comprovem que sigui un consum disponible, si no agafem el que introdu�m per defecte
		if (consEner == 'A'||consEner == 'B'||consEner == 'C'||consEner == 'D'||consEner == 'E'||consEner == 'F'){
			return consEner;
		}else {
			return 'F';
		}
	}
	
	public static String comprovarColor (String color) {
		//Comprovem que sigui un color disponible, si no agafem el que introdu�m per defecte
		if (color.equalsIgnoreCase("blanc") ||color.equalsIgnoreCase("negre")||color.equalsIgnoreCase("roig")||color.equalsIgnoreCase("blau")||color.equalsIgnoreCase("gris")){
			return color;
		}else {
			return "blanc";
		}
	}
	
	//Generem getters i setters
	public double getPreuBase() {
		return preuBase;
	}

	public void setPreuBase(double preuBase) {
		this.preuBase = preuBase;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public char getConsumEnergetic() {
		return consumEnergetic;
	}

	public void setConsumEnergetic(char consumEnergetic) {
		this.consumEnergetic = consumEnergetic;
	}

	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	
	//To string
	@Override
	public String toString() {
		return "Electrodomestic [preuBase=" + preuBase + ", color=" + color + ", consumEnergetic=" + consumEnergetic
				+ ", pes=" + pes + "]";
	}
	
	
	
	
	
}
