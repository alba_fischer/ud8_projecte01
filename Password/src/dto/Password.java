package dto;

import java.util.Random;

public class Password {
	//Atributs
	
	private int longitud;
	private String contrasenya;
	
	//CONSTRUCTORS
	
	//Constructor per defecte
	public Password() {
		this.longitud = 8;
		this.contrasenya = "";
	}
	
	//Constructor amb longitud per teclat i contrasenya aleatÚria
	public Password(int longitud) {
		this.longitud = longitud;		
		this.contrasenya = generarContrasenya(this.longitud);
	}
	
	private static final String ALFABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	public static String generarContrasenya (int longitud)  {
		//Generem una contrasenya aleatÚria
	    Random random = new Random();
	    StringBuilder contra = new StringBuilder(longitud);

	    for (int i = 0; i < longitud; i++) {
	        contra.append(ALFABET.charAt(random.nextInt(ALFABET.length())));
	    }

	    return contra.toString();
	    
	}

	// To string
	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contrasenya=" + contrasenya + "]";
	}

	//Getters and setters
	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public String getContrasenya() {
		return contrasenya;
	}

	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	
	
	
	
}
