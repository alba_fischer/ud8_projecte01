import java.util.Scanner;
import dto.Password;

public class PasswordApp {

	public static void main(String[] args) {
		// Comprovem que funcioni imprimint els objectes per pantalla

		
		Scanner scan = new Scanner(System.in);
		System.out.println("Introdueix la longitud de la contrasenya: ");
		int longitud = scan.nextInt();
		
		Password pass1 = new Password ();
		Password pass2 = new Password (longitud);
		
		System.out.println("Usem els diferents constructors de l'exercici:");
		System.out.println(pass1);
		System.out.println(pass2);
		scan.close();
	}

}
