import dto.Serie;

public class SerieApp {

	public static void main(String[] args) {
		// Comprovem que funcioni imprimint els objectes per pantalla
		
		Serie s1 = new Serie();
		Serie s2 = new Serie("How to get away with murder", "Peter Nowalk" );
		Serie s3 = new Serie("How to get away with murder", 6, "Drama legal, misteri","Peter Nowalk");
				
		System.out.println("Usem els diferents constructors de l'exercici:");
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

	}

}
