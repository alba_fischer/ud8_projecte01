package dto;

public class Serie {
	//Atributs
	
	private String titol;
	private int numTemporades;
	private boolean entregat;
	private String genere;
	private String creador;
	
	//CONSTRUCTORS
	
	//Constructor per defecte	
	public Serie() {
		this.numTemporades = 3;
		this.titol = "";
		this.entregat = false;
		this.genere = "";
		this.creador = "";
	}

	//Constructor amb titol i creador, la resta per defecte
	public Serie(String titol, String creador) {
		this.titol = titol;
		this.creador = creador;
		this.entregat = false;
		this.genere = "";
		this.numTemporades = 3;
	}

	//Constructor amb titol, nombre de temporades, genere i creador, entregat per defecte.
	public Serie(String titol, int numTemporades, String genere, String creador) {
		super();
		this.titol = titol;
		this.numTemporades = numTemporades;
		this.genere = genere;
		this.creador = creador;
		this.entregat = false;
	}

	
	//Generem getters and setters
	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public int getNumTemporades() {
		return numTemporades;
	}

	public void setNumTemporades(int numTemporades) {
		this.numTemporades = numTemporades;
	}

	public boolean isEntregat() {
		return entregat;
	}

	public void setEntregat(boolean entregat) {
		this.entregat = entregat;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	

	// to String
	@Override
	public String toString() {
		return "Serie [titol=" + titol + ", numTemporades=" + numTemporades + ", entregat=" + entregat + ", genere="
				+ genere + ", creador=" + creador + "]";
	}

	
}
