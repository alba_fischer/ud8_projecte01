import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		// Comprovem que funcioni imprimint els objectes per pantalla
		
		Persona p1 = new Persona();
		Persona p2 = new Persona("Alba", 23, 'F');
		Persona p3 = new Persona("Alba", 23, "74563892D",'F', 55, 1.6);
		
		System.out.println("Usem els diferents constructors de l'exercici:");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}

}
