package dto;

public class Persona {
	//Atributs
	
	private String nom; //Nom de la persona
	private int edat; //Edat de la persona
	private String dni; //DNI de la persona
	private char sexe; //Sexe de la persona
	private double pes; //Pes de la persona
	private double altura; //Altura de la persona
	
	//CONSTRUCTORS
	
	//Construtor per defecte.
	public Persona() {		
		this.nom = "";
		this.edat= 0;
		this.sexe = 'H';
		this.dni = "76598722F";
		this.pes = 0;
		this.altura = 0;
	}

	//Constructor amb nom, edat i sexe, la resta per defecte.
	public Persona(String nom, int edat, char sexe) {
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
		this.dni = "46593422F";
	}

	//Constructor amb tots els atributs.
	public Persona(String nom, int edat, String dni, char sexe, double pes, double altura) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.dni = dni;
		this.sexe = sexe;
		this.pes = pes;
		this.altura = altura;
	}
	
	//A partir d'aqu� l'exercici no ens ho demana per� ho fem per poder comprovar que funcioan b�
	//Getters
	public String getNom() {
		return nom;
	}

	public int getEdat() {
		return edat;
	}

	public String getDni() {
		return dni;
	}

	public char getSexe() {
		return sexe;
	}

	public double getPes() {
		return pes;
	}

	public double getAltura() {
		return altura;
	}

	//setters
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	// To string
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", edat=" + edat + ", dni=" + dni + ", sexe=" + sexe + ", pes=" + pes
				+ ", altura=" + altura + "]";
	}
	
	
	
}

